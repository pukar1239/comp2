﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Drawing;
using System.Text;
using System.Threading.Tasks;

namespace Programming_Drawing
{
    class Square : Shape
    {
        //declare variables
        int size;



        /// Parameter constructor        
        public Square(int x, int y)
        {

        }

        //over-ride Shape class draw
        public override void draw(Graphics g, Color c, int thickness)
        {
            Pen p = new Pen(c, thickness);
            g.DrawRectangle(p, x, y, size, size);
        }

        /// another parameter constructor        

        public Square() : base()
        {


        }
        //set square area
        public void setSquare(int len)
        {
            this.size = len;
        }

        public int getSquare()
        {
            return this.size;
        }


    }
}
