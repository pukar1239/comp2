﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Drawing;
using System.Text;
using System.Threading.Tasks;

namespace Programming_Drawing
{
    public class Circle : Shape
    {

        /// <summary>
        /// Radius declaration
        /// </summary>
        int radius;

        /// <summary>
        ///  parameterized constructor
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="radius"></param>
        public Circle(int x, int y, int radius) : base(x, y)
        {
            this.radius = radius;

        }

        /// <summary>
        /// default constructor
        /// </summary>
        public Circle()
        {

        }

        public Circle(int radius)
        {
            this.radius = radius;
        }

        /// <summary>
        /// Parameterized constructor
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        public Circle(int x, int y) : base(x, y)
        {

        }

        /// <summary>
        /// draw method 
        /// </summary>
        /// <param name="g"></param>
        /// <param name="c"></param>
        /// <param name="thickness"></param>
        public override void draw(Graphics g, Color c, int thickness)
        {
            Pen p = new Pen(c, thickness);
            g.DrawEllipse(p, x, y, radius, radius);
        }
        /// <summary>
        /// Set Radious value
        /// </summary>
        /// <param name="radius"></param>
        public void setRadius(int radius)
        {
            this.radius = radius;
        }

        /// <summary>
        /// getRadious Value
        /// </summary>
        /// <returns></returns>
        public int getRadius()
        {
            return this.radius;
        }
    }
}
