﻿
namespace Programming_Drawing
{
    partial class Comp_2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Execute = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.txtCmdline = new System.Windows.Forms.TextBox();
            this.txtCommand = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // Execute
            // 
            this.Execute.Location = new System.Drawing.Point(131, 278);
            this.Execute.Name = "Execute";
            this.Execute.Size = new System.Drawing.Size(75, 23);
            this.Execute.TabIndex = 7;
            this.Execute.Text = "Execute";
            this.Execute.UseVisualStyleBackColor = true;
            this.Execute.Click += new System.EventHandler(this.Execute_Click);
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Location = new System.Drawing.Point(233, 38);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(245, 263);
            this.panel1.TabIndex = 6;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint_1);
            // 
            // txtCmdline
            // 
            this.txtCmdline.Location = new System.Drawing.Point(19, 44);
            this.txtCmdline.Multiline = true;
            this.txtCmdline.Name = "txtCmdline";
            this.txtCmdline.Size = new System.Drawing.Size(187, 218);
            this.txtCmdline.TabIndex = 5;
            // 
            // txtCommand
            // 
            this.txtCommand.Location = new System.Drawing.Point(24, 279);
            this.txtCommand.Name = "txtCommand";
            this.txtCommand.Size = new System.Drawing.Size(100, 20);
            this.txtCommand.TabIndex = 4;
            this.txtCommand.Text = "run";
            // 
            // Comp_2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(497, 323);
            this.Controls.Add(this.Execute);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.txtCmdline);
            this.Controls.Add(this.txtCommand);
            this.Name = "Comp_2";
            this.Text = "Comp_2";
            this.Load += new System.EventHandler(this.Comp_2_Load_1);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Execute;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox txtCmdline;
        private System.Windows.Forms.TextBox txtCommand;
    }
}