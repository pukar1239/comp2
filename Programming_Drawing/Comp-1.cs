﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Programming_Drawing
{
    public partial class Comp_1 : Form
    {
        public Comp_1()
        {
            InitializeComponent();
        }

        


        Boolean drawCircle, drawRect, drawPolgon, drawSqr; //boolean value for checking shapes
        String program;
        String[] words;
        int moveX, moveY;
        int thickness;

        List<Circle> circleObjects;
        List<Rectangle> rectangleObjects;
        List<Move> moveObjects;
        List<Polygon> polygonObjects;
        List<Square> squareObjects;

        private void Comp_1_Load(object sender, EventArgs e)
        {
            Circle circle = new Circle();
            circleObjects = new List<Circle>();
            moveObjects = new List<Move>();
            Rectangle rectangle = new Rectangle();
            rectangleObjects = new List<Rectangle>();
            Square sqr = new Square();
            squareObjects = new List<Square>();
            c = Color.DarkRed;
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;
            if (drawCircle == true)//draw circle condition
            {
                foreach (Circle circleObject in circleObjects)
                {
                    console_text += "Drawing Circle\n\n";
                    circleObject.draw(g, c, thickness); //draw circle with given graphics
                }
            }

            if (drawRect == true) //draw rectangle condition
            {
                foreach (Rectangle rectangleObject in rectangleObjects)
                {
                    console_text += "Drawing Rectangle\n\n";
                    rectangleObject.draw(g, c, thickness); //draw circle with given graphics
                }
            }

            if (drawSqr == true) //draw square condition
            {
                foreach (Square squareObject in squareObjects)
                {
                    console_text += "Drawing Square\n\n";
                    squareObject.draw(g, c, thickness); //draw circle with given graphics
                }
            }

            if (drawPolgon == true)
            {
                Pen blackPen = new Pen(c, thickness);
                PointF point1 = new PointF(50.0F, 50.0F);
                PointF point2 = new PointF(40.0F, 25.0F);
                PointF point3 = new PointF(60.0F, 5.0F);
                PointF point4 = new PointF(80.0F, 20.0F);
                PointF point5 = new PointF(100.0F, 50.0F);
                PointF point6 = new PointF(110.0F, 100.0F);
                PointF point7 = new PointF(130.0F, 150.0F);
                string[] str = new string[5];
                PointF[] curvePoints =
                 {
                     point1,
                     point2,
                     point3,
                     point4,
                     point5,
                     point6,
                     point7
                 };
                e.Graphics.DrawPolygon(blackPen, curvePoints);
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "Text Files|*.txt";

            if (sfd.ShowDialog() == DialogResult.OK)
            {
                System.IO.File.WriteAllText(sfd.FileName, txtCmdline.Text);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "Text Files|*.txt";

            if (ofd.ShowDialog() == DialogResult.OK)
            {
                txtCmdline.Text = System.IO.File.ReadAllText(ofd.FileName);
            }
        }

        Color c;
        Point point;
        string actionCmd;
        string console_text;


        private void Execute_Click(object sender, EventArgs e)
        {
            if (txtCommand.Text == "")
            {
                MessageBox.Show("Invalid Command!!");
            }
            else if (txtCmdline.Text == "")
            {
                MessageBox.Show("Invalid Command!!");
            }
            else if (txtCommand.Text == "run" | txtCommand.Text == "clear" | txtCommand.Text == "reset")
            {
                MessageBox.Show(txtCmdline.Text);
                switch (txtCommand.Text)
                {
                    case "run":
                        try
                        {
                            program = txtCmdline.Text.ToLower();
                            char[] delimiters = new char[] { '\r', '\n' };
                            string[] parts = program.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                            console_text = "Program code: \n";
                            foreach (string part in parts)
                            {
                                console_text += part + "\n";
                            }
                            console_text += "\n\n";



                            for (int i = 0; i < parts.Length; i++)
                            {
                                //single code line
                                String code_line = parts[i];

                                char[] code_delimiters = new char[] { ' ' };
                                words = code_line.Split(code_delimiters, StringSplitOptions.RemoveEmptyEntries);

                                //condition to check if "draw" then
                                if (words[0].Equals("draw"))
                                {
                                    if (words[1] == "circle") // condition to check if "circle" then
                                    {
                                        if (!(words.Length == 3)) //checks syntax
                                        {
                                            MessageBox.Show("Enter correct command and parameter");
                                            console_text += "View the correct Command: \n e.g. draw circle 100 or draw circle r \n\n";
                                        }
                                        else
                                        {

                                            //Create a new circle
                                            Circle circle = new Circle();
                                            circle.setX(moveX);
                                            circle.setY(moveY);
                                            circle.setRadius(Convert.ToInt32(words[2]));
                                            circleObjects.Add(circle);
                                            drawCircle = true;
                                            console_text += "Adding new circle\n\n";

                                        }
                                    }
                                    if (words[1].Equals("rectangle")) // 
                                    {
                                        //MessageBox.Show(moveX.ToString());
                                        if (!(words.Length == 4))
                                        {

                                            console_text += "View the correct Command:: \n e.g. draw rectangle 100 100 or draw circle h w \n\n";
                                            MessageBox.Show(console_text);
                                        }
                                        else
                                        {
                                            //create a new rectangle
                                            Rectangle rect = new Rectangle();
                                            rect.setX(moveX);
                                            rect.setY(moveY);
                                            rect.setHeight(Convert.ToInt32(words[2]));
                                            rect.setWidth(Convert.ToInt32(words[3]));
                                            rectangleObjects.Add(rect);
                                            drawRect = true;
                                            console_text += "Adding new rectangle\n\n";

                                        }
                                    }
                                    if (words[1].Equals("square")) // 
                                    {
                                        //MessageBox.Show(moveX.ToString());
                                        if (!(words.Length == 3))
                                        {

                                            console_text += "View the correct Command:: \n e.g. draw square 100 or draw circle 50 \n\n";
                                            MessageBox.Show(console_text, "Enter correct command");
                                        }
                                        else
                                        {
                                            //create a new square
                                            Square sqr = new Square();
                                            sqr.setX(moveX);
                                            sqr.setY(moveY);
                                            sqr.setSquare(Convert.ToInt32(words[2]));

                                            squareObjects.Add(sqr);
                                            drawSqr = true;


                                        }
                                    }


                                    if (words[1].Equals("polygon"))
                                    {
                                        drawPolgon = true;
                                    }
                                }
                                if (words[0] == "move")
                                {

                                    moveX = Convert.ToInt32(words[1]);
                                    moveY = Convert.ToInt32(words[2]);
                                    console_text += "X=" + moveX + "\n" + "Y=" + moveY + "\n\n";

                                }
                                if (words[0] == "color")
                                {
                                    thickness = Convert.ToInt32(words[2]);

                                    if (words[1] == "red")
                                    {
                                        c = Color.Red;
                                        console_text += "Pen is of red color\n\n";
                                    }
                                    else if (words[1] == "blue")
                                    {
                                        c = Color.Blue;
                                        console_text += "Pen is of blue color\n\n";
                                    }
                                    else if (words[1] == "yellow")
                                    {
                                        c = Color.Yellow;
                                        console_text += "Pen is of yellow color\n\n";
                                    }
                                    else
                                    {
                                        c = Color.Green;
                                        console_text += "Pen is of green color\n\n";
                                    }
                                }
                            }
                        }
                        catch (IndexOutOfRangeException ex)
                        {
                            console_text += "Error: " + ex.Message + "\n\n";
                        }
                        catch (FormatException ex)
                        {
                            console_text += "!!Please input correct parameter!!\n\n";
                        }
                        catch (ArgumentOutOfRangeException ex)
                        {
                            console_text += "!!Please input correct parameter!!\n\n";
                        }
                        panel1.Refresh(); //refresh 
                        break;
                    case "clear":
                        txtCmdline.Clear();
                        txtCommand.Clear();

                        panel1.Refresh();
                        break;
                    case "reset":
                        txtCommand.Clear();
                        txtCmdline.Clear();
                        c = Color.Green;
                        moveX = 0;
                        moveY = 0;
                        panel1.Refresh();
                        break;
                    default:
                        MessageBox.Show("The action command is empty\n" +
                            "Or\n" +
                            "Must be: 'Run' for Execuit the program\n" +
                            "Must be: 'Clear' for Fresh Start");
                        break;
                }


            }
            else
            {
                MessageBox.Show("Program error");
            }

        }
    }
}
