﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Programming_Drawing
{
    public partial class Home : Form
    {
        public Home()
        {
            InitializeComponent();
        }

        private void comp1ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Comp_1 cmp1 = new Comp_1();
            cmp1.MdiParent = this;
            cmp1.Show();
        }

        private void comp2ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Comp_2 cm2 = new Comp_2();
            cm2.MdiParent = this;
            cm2.Show();
        }
    }
}
