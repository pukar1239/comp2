﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Programming_Drawing
{
    public partial class Comp_2 : Form
    {
        public Comp_2()
        {
            InitializeComponent();
        }
        Color c;
        Point point;
        string actionCmd;
        string console_text;
        String Error;
        Boolean error = false;

        Circle circle;
        Square square;
        Rectangle rectangle;


        Boolean drawCircle, drawRect, drawSqr; //boolean value for checking shapes
        String program;
        String[] words;
        int moveX, moveY;
        int thickness;

        int loopSize;
        Boolean loopStart = false;
        List<string> loopContent;

        int height, width, radious, squaresize;
        Boolean method;

        List<Circle> circleObjects;

        private void Comp_2_Load(object sender, EventArgs e)
        {
            Circle circle = new Circle();
            circleObjects = new List<Circle>();
            moveObjects = new List<Move>();
            Rectangle rectangle = new Rectangle();
            rectangleObjects = new List<Rectangle>();
            Square sqr = new Square();
            squareObjects = new List<Square>();
            c = Color.DarkRed;
        }

        private void Comp_2_Load_1(object sender, EventArgs e)
        {
            Circle circle = new Circle();
            circleObjects = new List<Circle>();
            moveObjects = new List<Move>();
            Rectangle rectangle = new Rectangle();
            rectangleObjects = new List<Rectangle>();
            Square sqr = new Square();
            squareObjects = new List<Square>();
            c = Color.DarkRed;
        }

        private void panel1_Paint_1(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;
            if (drawCircle == true)//draw circle condition
            {
                foreach (Circle circleObject in circleObjects)
                {
                    console_text += "Drawing Circle\n\n";
                    circleObject.draw(g, c, thickness); //draw circle with given graphics
                }
            }

            if (drawRect == true) //draw rectangle condition
            {
                foreach (Rectangle rectangleObject in rectangleObjects)
                {
                    console_text += "Drawing Rectangle\n\n";
                    rectangleObject.draw(g, c, thickness); //draw circle with given graphics
                }
            }

            if (drawSqr == true) //draw square condition
            {
                foreach (Square squareObject in squareObjects)
                {
                    console_text += "Drawing Square\n\n";
                    squareObject.draw(g, c, thickness); //draw circle with given graphics
                }
            }
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {
            
        }

        List<Rectangle> rectangleObjects;
        List<Move> moveObjects;
        List<Polygon> polygonObjects;
        List<Square> squareObjects;

        private void Execute_Click(object sender, EventArgs e)
        {
            int temp;
            MessageBox.Show(txtCmdline.Text);
            switch (txtCommand.Text)
            {
                case "run":
                    try
                    {
                        program = txtCmdline.Text.ToLower();
                        char[] delimiters = new char[] { '\r', '\n' };
                        string[] parts = program.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                        console_text = "Program code: \n";
                        loopContent = new List<string>();
                        foreach (string part in parts)
                        {
                            console_text += part + "\n";
                        }
                        console_text += "\n\n";
                        for (int i = 0; i < parts.Length; i++)
                        {

                            if (this.loopStart == true)
                            {
                                loopContent.Add(parts[i]);
                            }
                            else
                            {
                                
                                
                            }
                        }

                        for (int k = 0; k < loopSize; k++)
                        {
                            for (int j = 0; j < loopContent.Count - 1; j++)
                            {
                               
                                
                            }

                        }


                        for (int i = 0; i < parts.Length; i++)
                        {
                            //single code line
                            String code_line = parts[i];

                            char[] code_delimiters = new char[] { ' ' };
                            words = code_line.Split(code_delimiters, StringSplitOptions.RemoveEmptyEntries);

                            //condition to check if "draw" then
                            if (words[0].Equals("draw"))
                            {
                                if (words[1] == "circle") // condition to check if "circle" then
                                {
                                    if (!(words.Length == 3))
                                    {
                                        error = true;
                                        Error = "The error,please type correct code for 'draw circle'";
                                        MessageBox.Show(Error);

                                    }
                                    else if (Int32.TryParse(words[2], out temp))
                                    {
                                        Circle circle = new Circle();
                                        circle.setX(moveX);
                                        circle.setY(moveY);
                                        circle.setRadius(Convert.ToInt32(words[2]));

                                        circleObjects.Add(circle);
                                        drawCircle = true;
                                        console_text += "Adding new circle\n\n";
                                        //return null;
                                    }
                                    else if (words[2] == "radious")
                                    {
                                        Circle circle = new Circle();
                                        circle.setX(moveX);
                                        circle.setY(moveY);

                                        circle.setRadius(this.radious);
                                        circleObjects.Add(circle);
                                        drawCircle = true;
                                        console_text += "Adding new circle\n\n";

                                    }

                                    else
                                    {
                                        MessageBox.Show("Please enter The parameters in integers . ");


                                    }
                                }

                                if (words[1].Equals("rectangle")) // 
                                {
                                    //MessageBox.Show(moveX.ToString());
                                    if (!(words.Length == 4))
                                    {

                                        console_text += "View the correct Command:: \n e.g. draw rectangle 100 100 or draw circle h w \n\n";
                                        MessageBox.Show(console_text);
                                    }
                                    else
                                    {
                                        //create a new rectangle
                                        Rectangle rect = new Rectangle();
                                        rect.setX(moveX);
                                        rect.setY(moveY);
                                        rect.setHeight(Convert.ToInt32(words[2]));
                                        rect.setWidth(Convert.ToInt32(words[3]));
                                        rectangleObjects.Add(rect);
                                        drawRect = true;
                                        console_text += "Adding new rectangle\n\n";

                                    }
                                }

                                if (words[1].Equals("square")) // 
                                {
                                    //MessageBox.Show(moveX.ToString());
                                    if (!(words.Length == 3))
                                    {
                                        error = true;
                                        Error = "error,  Please type correct code for 'draw square'";

                                        MessageBox.Show(Error);
                                    }
                                    else if (Int32.TryParse(words[2], out temp))
                                    {
                                        Square sqr = new Square();
                                        sqr.setX(moveX);
                                        sqr.setY(moveY);
                                        sqr.setSquare(Convert.ToInt32(words[2]));

                                        squareObjects.Add(sqr);
                                        console_text += "Adding new square\n\n";
                                        drawSqr = true;
                                    }
                                    else if (words[2] == "squaresize")
                                    {
                                        Square sqr = new Square();
                                        sqr.setX(moveX);
                                        sqr.setY(moveY);
                                        sqr.setSquare(this.squaresize);
                                        console_text += "Adding new square\n\n";
                                        squareObjects.Add(sqr);
                                        drawSqr = true;

                                    }
                                    else
                                    {
                                        MessageBox.Show("Please enter The parameters in integers in line ");


                                    }
                                }



                            }
                            else if (words[0] == "loop")
                            {
                                this.loopStart = true;
                                this.loopSize = Convert.ToInt32(words[1]);
                            }
                            else if (words[0] == "endloop")
                            {
                                loopStart = false;
                                loopSize = 0;
                            }
                            else if (words[0] == "radious")
                            {
                                if (words[1] == "=" && Int32.TryParse(words[2], out temp))
                                {
                                    this.radious = Convert.ToInt32(words[2]);
                                }
                                else if (words[1] == "+=")
                                {
                                    this.radious = radious + Convert.ToInt32(words[2]);
                                }
                                else
                                {
                                    MessageBox.Show("Please enter Correct Parameters");

                                }

                            }
                            else if (words[0] == "height")
                            {
                                if (words[1] == "=" && Int32.TryParse(words[2], out temp))
                                {
                                    this.height = Convert.ToInt32(words[2]);
                                }
                                else if (words[1] == "+=")
                                {
                                    this.height = height + Convert.ToInt32(words[2]);
                                }
                                else
                                {
                                    MessageBox.Show("Please enter Correct Parameters");

                                }

                            }
                            
                            else if (words[0] == "width")
                            {
                                if (words[1] == "=" && Int32.TryParse(words[2], out temp))
                                {
                                    this.width = Convert.ToInt32(words[2]);
                                }
                                else if (words[1] == "+=")
                                {
                                    this.width = width + Convert.ToInt32(words[2]);
                                }
                                else
                                {
                                    MessageBox.Show("Please enter Correct Parameters");

                                }

                            }
                            else if (words[0] == "squaresize")
                            {
                                if (words[1] == "=" && Int32.TryParse(words[2], out temp))
                                {
                                    this.squaresize = Convert.ToInt32(words[2]);
                                }
                                else if (words[1] == "+=")
                                {
                                    this.squaresize = squaresize + Convert.ToInt32(words[2]);
                                }
                                else
                                {
                                    MessageBox.Show("Please enter Correct Parameters");

                                }

                            }
                            else if (words[0] == "moveto")
                            {
                                if (!(words.Length == 3))
                                {
                                    error = true;
                                    Error = ", Please type correct code for 'move'";

                                    MessageBox.Show(Error);
                                }
                                else if (Int32.TryParse(words[1], out temp) && Int32.TryParse(words[2], out temp))
                                {
                                    this.moveX = Convert.ToInt32(words[1]);
                                    this.moveY = Convert.ToInt32(words[2]);
                                }
                                else
                                {
                                    MessageBox.Show("Please enter The parameters in integers in line ");


                                }
                            }

                            else if (words[0] == "move")
                            {

                                moveX = Convert.ToInt32(words[1]);
                                moveY = Convert.ToInt32(words[2]);
                                console_text += "X=" + moveX + "\n" + "Y=" + moveY + "\n\n";

                            }
                            else if (words[0] == "color")
                            {
                                thickness = Convert.ToInt32(words[2]);

                                if (words[1] == "red")
                                {
                                    c = Color.Red;
                                    console_text += "Pen is of red color\n\n";
                                }
                                else if (words[1] == "blue")
                                {
                                    c = Color.Blue;
                                    console_text += "Pen is of blue color\n\n";
                                }
                                else if (words[1] == "yellow")
                                {
                                    c = Color.Yellow;
                                    console_text += "Pen is of yellow color\n\n";
                                }
                                else
                                {
                                    c = Color.Green;
                                    console_text += "Pen is of green color\n\n";
                                }
                            }
                        }
                    }
                    catch (IndexOutOfRangeException ex)
                    {
                        console_text += "Error: " + ex.Message + "\n\n";
                    }
                    catch (FormatException ex)
                    {
                        console_text += "!!Please input correct parameter!!\n\n";
                    }
                    catch (ArgumentOutOfRangeException ex)
                    {
                        console_text += "!!Please input correct parameter!!\n\n";
                    }
                    panel1.Refresh(); //refresh 
                    break;
                case "clear":
                    txtCmdline.Clear();

                    panel1.Refresh();
                    break;
                case "reset":

                    txtCmdline.Clear();
                    c = Color.Green;
                    moveX = 0;
                    moveY = 0;
                    panel1.Refresh();
                    break;
                default:
                    MessageBox.Show("The action command is empty\n" +
                        "Or\n" +
                        "Must be: 'Run' for Execuit the program\n" +
                        "Must be: 'Clear' for Fresh Start");
                    break;
            }


        }
    }
}
